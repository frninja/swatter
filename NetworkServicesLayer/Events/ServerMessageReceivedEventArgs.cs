﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkServicesLayer.Events
{
    public class ServerMessageReceivedEventArgs : EventArgs 
    {
        public string Message { get; private set; }

        public ServerMessageReceivedEventArgs(string message)
        {
            Message = message;
        }
    }
}
