﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkServicesLayer
{
    public class CommandFactory
    {
        public static string MakeClientGameStartedCommand()
        {
            return new StringBuilder()
                .Append(CommandHelper.ClientGameStarted)
                .Append(CommandHelper.CommandDelimeter)
                .Append(CommandHelper.CommandTerminator)
                .ToString();
        }

        public static string MakeClientWonCommand()
        {
            return new StringBuilder()
                .Append(CommandHelper.ClientWonCommand)
                .Append(CommandHelper.CommandDelimeter)
                .Append(CommandHelper.CommandTerminator)
                .ToString();
        }

        public static string MakeFlyMoveCommand(int x, int y)
        {
            return new StringBuilder()
                .Append(CommandHelper.FlyMoveCommand)
                .Append(CommandHelper.CommandDelimeter)
                .Append(x)
                .Append(CommandHelper.CommandDelimeter)
                .Append(y)
                .Append(CommandHelper.CommandDelimeter)
                .Append(CommandHelper.CommandTerminator)
                .ToString();
        }

        public static string MakeSwatterMoveCommand(int x, int y)
        {
            return new StringBuilder()
                .Append(CommandHelper.SwatterMoveCommand)
                .Append(CommandHelper.CommandDelimeter)
                .Append(x)
                .Append(CommandHelper.CommandDelimeter)
                .Append(y)
                .Append(CommandHelper.CommandDelimeter)
                .Append(CommandHelper.CommandTerminator)
                .ToString();
        }
    }
}
