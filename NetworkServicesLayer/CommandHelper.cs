﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkServicesLayer
{
    public class CommandHelper
    {
        public static char CommandDelimeter = ' ';
        public static string CommandTerminator = "<EOC>";

        public static string ClientGameStarted = "CLIENT_START";
        public static string ClientWonCommand = "CLIENT_WON";
        public static string FlyMoveCommand = "FLY_MOVE";
        public static string SwatterMoveCommand = "SWATTER_MOVE";
    }
}
