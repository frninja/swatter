﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkServicesLayer
{
    public enum CommandKind
    {
        ClientGameStarted,
        ClientWon,
        FlyMove,
        SwatterMove,
        Unknown
    }
}
