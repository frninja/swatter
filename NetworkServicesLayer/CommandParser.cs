﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkServicesLayer
{
    public class CommandParser
    {

        public static CommandKind Parse(string message, out Command cmd)
        {
            cmd = null;

            if (message.Length > 0 && message.EndsWith(CommandHelper.CommandTerminator))
            {
                IEnumerable<string> parts = message.Split(new char[] { CommandHelper.CommandDelimeter }, StringSplitOptions.RemoveEmptyEntries);

                // Parse command
                string command = parts.First();

                CommandKind commandKind = ParseCommand(command);
                if (commandKind == CommandKind.Unknown)
                    return CommandKind.Unknown;

                // Parse cmd args
                parts = parts.Skip(1).ToArray();
                

                switch (commandKind)
                {
                    case CommandKind.FlyMove:
                    case CommandKind.SwatterMove:
                        Position pos;

                        string[] coords = parts.Take(2).ToArray();
                        parts = parts.Skip(2);

                        if (!ParsePosition(coords, out pos))
                            return CommandKind.Unknown;

                        cmd = new Command(commandKind, pos);
                        break;
                }

                // Check unexpected args
                if (parts.ToArray().Length != 1)
                    return CommandKind.Unknown;

                return commandKind;
            }

            return CommandKind.Unknown;
        }

        private static CommandKind ParseCommand(string command)
        {
            if (command == CommandHelper.FlyMoveCommand)
                return CommandKind.FlyMove;
            else if (command == CommandHelper.SwatterMoveCommand)
                return CommandKind.SwatterMove;
            else if (command == CommandHelper.ClientWonCommand)
                return CommandKind.ClientWon;
            else if (command == CommandHelper.ClientGameStarted)
                return CommandKind.ClientGameStarted;
            else
                return CommandKind.Unknown;

        }

        private static bool ParsePosition(string[] coords, out Position pos) 
        {
            pos = null;

            int x, y;
            if (int.TryParse(coords[0], out x) && int.TryParse(coords[1], out y))
            {
                pos = new Position(x, y);
                return true;
            }

            return false;
        }
    }
}
