﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Threading;

using System.Net;
using System.Net.Sockets;

using NetworkServicesLayer.Events;

namespace NetworkServicesLayer
{
    public class GameClient : IGameClient
    {
        private IPAddress _serverIpAddress;
        private int _serverPort;

        // 
        private Thread _serverListenerThread;
        private TcpClient _serverListener;

        public event EventHandler<FlyLocationChangedEventArgs> FlyLocationChanged;

        public void Initialize(IPAddress serverIpAddress, int serverPort)
        {
            _serverIpAddress = serverIpAddress;
            _serverPort = serverPort;

            _serverListenerThread = new Thread(this.RunServerListener);
            _serverListenerThread.Start();
        }

        public async void SendMessage(string message)
        {
            try
            {
                NetworkStream stream = _serverListener.GetStream();

                byte[] messageBytes = Encoding.UTF8.GetBytes(message);
                await stream.WriteAsync(messageBytes, 0, messageBytes.Length);
            }
            catch (Exception e)
            {
                Console.WriteLine("[SEND MESSAGE] Failed: {0}", e.Message);
            }
        }

        private async void RunServerListener()
        {
            _serverListener = new TcpClient();
            try
            {
                await _serverListener.ConnectAsync(_serverIpAddress, _serverPort);

                Console.WriteLine("Connected to server!");

                while (true)
                {
                    var networkStream = _serverListener.GetStream();

                    byte[] buffer = new byte[4096];
                    int byteCount = await networkStream.ReadAsync(buffer, 0, buffer.Length);

                    string reply = Encoding.UTF8.GetString(buffer, 0, byteCount);

                    Console.WriteLine("Got from server: {0}", reply);

                    ProcessServerMessage(reply);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("[LISTEN] Failed: {0}", e.Message);
            }

        }


        private void ProcessServerMessage(string message)
        {
            Command cmd = null;
            CommandKind cmdKind = CommandParser.Parse(message, out cmd);

            if (cmdKind == CommandKind.Unknown)
                return;

            switch (cmdKind)
            {
                case CommandKind.FlyMove:
                    if (FlyLocationChanged != null)
                    {
                        FlyLocationChanged(this, new FlyLocationChangedEventArgs(cmd.Position.X, cmd.Position.Y));
                    }
                    break;
            }
        }
    }
}
