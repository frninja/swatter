﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Net;

using NetworkServicesLayer.Events;

namespace NetworkServicesLayer
{
    public interface IGameClient
    {
        event EventHandler<FlyLocationChangedEventArgs> FlyLocationChanged;

        void Initialize(IPAddress serverIpAddress, int port);

        void SendMessage(string message);
    }
}
