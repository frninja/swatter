﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkServicesLayer
{
    public class Command
    {
        public CommandKind Kind { get; private set; }

        public Position Position { get; private set; }

        public Command(CommandKind kind)
        {
            Kind = kind;
        }

        public Command(CommandKind kind, Position position)
        {
            Kind = kind;
            Position = position;
        }
    }
}
