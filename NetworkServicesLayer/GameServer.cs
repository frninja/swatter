﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Threading;

using System.Net;
using System.Net.Sockets;

using NetworkServicesLayer.Events;

namespace NetworkServicesLayer
{
    public class GameServer : IGameServer
    {
        private readonly int _port = 55783;
        private readonly int _bufferSize = 4096;


        // Thread for processing incoming control messages from client
        // private Thread _clientListenerThread;

        // Tcp client for outcoming control messages
        //private TcpClient _tcpClient;

        private List<Client> _clients = new List<Client>();

        public event Action ClientGameStarted;
        public event EventHandler<ClientWonEventArgs> ClientWon;
        public event EventHandler<SwatterLocationChangedEventArgs> SwatterLocationChanged;


        public void Initialize()
        {
            //_clientListenerThread = new Thread(this.RunClientListener);
            //_clientListenerThread.Start();

            this.RunClientListenerAsync();
        }

        public async void SendMessage(string message)
        {
            foreach (Client client in _clients)
            {
                try
                {
                    await client.Send(message);
                }
                catch (Exception e)
                {
                    Console.WriteLine("[SERVER SEND MESSAGE] Failed: {0}", e.Message);
                }
            }

        }

        private async Task ProcessClientTearOff(TcpClient c)
        {
            using (var client = new Client(c))
            {
                _clients.Add(client);

                client.ClientGameStarted += () =>
                    {
                        if (ClientGameStarted != null)
                            ClientGameStarted();
                    };


                client.SwatterLocationChanged += (sender, e) =>
                    {
                        if (SwatterLocationChanged != null)
                            SwatterLocationChanged(this, e);
                    };

                client.ClientWon += (sender, e) =>
                    {
                        if (ClientWon != null)
                            ClientWon(this, new ClientWonEventArgs());
                    };

                await client.ProcessAsync();
            }
        }

        private async void RunClientListenerAsync()
        {
            var ipAddress = IPAddress.Parse("127.0.0.1");

            try
            {
                // Слушаем служебные сообщения - клиент подключен, отключен, выиграл, проиграл
                var tcpListener = TcpListener.Create(_port);
                tcpListener.Start();

                Console.WriteLine("Server started!");

                // TODO: Call started event!

                while (true)
                {
                    TcpClient tcpClient = await tcpListener.AcceptTcpClientAsync();
                    Console.WriteLine("Client connected!");

                    this.ProcessClientTearOff(tcpClient);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("[LISTEN] Failed: {0}", e.Message);
            }

        }

    }
}
