﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Net.Sockets;

using NetworkServicesLayer.Events;

namespace NetworkServicesLayer
{
    public class Client : IDisposable
    {
        private NetworkStream _stream;

        private readonly int BufferSize = 4096;

        private readonly Guid _guid;

        public event Action ClientGameStarted;
        public event EventHandler<SwatterLocationChangedEventArgs> SwatterLocationChanged;
        public event EventHandler<ClientWonEventArgs> ClientWon;

        public Client(TcpClient client)
        {
            _stream = client.GetStream();

            _guid = Guid.NewGuid();
            Console.WriteLine("Client [{0}] created", _guid);
        }

        public void Dispose()
        {
            _stream.Dispose();
        }

        public async Task Send(string message)
        {
            byte[] data = Encoding.UTF8.GetBytes(message);

            Console.WriteLine("Client [{0}] sending data: {1}", _guid, message);

            await _stream.WriteAsync(data, 0, data.Length);

            Console.WriteLine("Client [{0}] sent data: {1}", _guid, message);
        }

        public async Task ProcessAsync()
        {
            while (true)
            {
                byte[] buffer = new byte[BufferSize];
                int bytesCount = await _stream.ReadAsync(buffer, 0, buffer.Length);

                string data = Encoding.UTF8.GetString(buffer, 0, bytesCount);

                Console.WriteLine("[SERVER] got data from Client [{0}] {1}", _guid, data);

                ProcessClientMessage(data);

                //string response = "BANG!!!";
                //byte[] responseData = Encoding.UTF8.GetBytes(response);

                //await _stream.WriteAsync(responseData, 0, responseData.Length);

                //Console.WriteLine("Client [{0}] sent data: {1}", _guid, response);
            }
        }

        private void ProcessClientMessage(string message)
        {
            Command cmd = null;
            CommandKind cmdKind = CommandParser.Parse(message, out cmd);

            if (cmdKind == CommandKind.Unknown)
                return;

            switch (cmdKind)
            {
                case CommandKind.ClientGameStarted:
                    if (ClientGameStarted != null)
                    {
                        ClientGameStarted();
                    }
                    break;
                case CommandKind.ClientWon:
                    if (ClientWon != null)
                    {
                        ClientWon(this, new ClientWonEventArgs());
                    }
                    break;
                case CommandKind.SwatterMove:
                    if (SwatterLocationChanged != null)
                    {
                        SwatterLocationChanged(this, new SwatterLocationChangedEventArgs(cmd.Position.X, cmd.Position.Y));
                    }
                    break;
            }
        }
    }
}
