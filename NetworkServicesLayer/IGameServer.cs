﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NetworkServicesLayer.Events;

namespace NetworkServicesLayer
{
    public interface IGameServer
    {
        event Action ClientGameStarted;
        event EventHandler<ClientWonEventArgs> ClientWon;
        event EventHandler<SwatterLocationChangedEventArgs> SwatterLocationChanged;

        void Initialize();

        void SendMessage(string message);
    }
}
