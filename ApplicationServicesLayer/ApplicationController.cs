﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using StructureMap;

namespace ApplicationServicesLayer
{
    public class ApplicationController : IApplicationController
    {
        private readonly IContainer _container;

        public ApplicationController()
        {
            _container = new Container(x => {
                x.For<IApplicationController>().Singleton().Use<ApplicationController>(this);
            });
        }

        public void Configure(Action<ConfigurationExpression> configure)
        {
            _container.Configure(configure);
        }

        public TInstance GetInstance<TInstance>()
        {
            return _container.GetInstance<TInstance>();
        }
    }
}
