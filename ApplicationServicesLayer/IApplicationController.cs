﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using StructureMap;

namespace ApplicationServicesLayer
{
    public interface IApplicationController
    {
        void Configure(Action<ConfigurationExpression> configure);

        TInstance GetInstance<TInstance>();
    }
}
