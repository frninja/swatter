﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserInterfaceLayer.Events
{
    public class GameRoleSelectedEventArgs : EventArgs
    {
        public GameRole Role { get; private set; }

        public GameRoleSelectedEventArgs(GameRole role)
        {
            Role = role;
        }
    }
}
