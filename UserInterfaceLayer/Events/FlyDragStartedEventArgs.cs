﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserInterfaceLayer.Events
{
    public class FlyDragStartedEventArgs : EventArgs
    {
        public int X { get; private set; }
        public int Y { get; private set; }

        public FlyDragStartedEventArgs(int x, int y)
        {
            X = x;
            Y = y;
        }
    }
}
