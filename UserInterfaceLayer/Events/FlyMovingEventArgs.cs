﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserInterfaceLayer.Events
{
    public class FlyMovingEventArgs : EventArgs
    {
        public int X { get; private set; }
        public int Y { get; private set; }

        public FlyMovingEventArgs(int x, int y)
        {
            X = x;
            Y = y;
        }
    }
}
