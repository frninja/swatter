﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NetworkServicesLayer.Events;

namespace UserInterfaceLayer.Views
{
    public interface IGameClientView : IView
    {
        event Action TimerTick;
        event Action ClientGameStarted;
        event EventHandler<ClientWonEventArgs> ClientWon;
        event EventHandler<SwatterLocationChangedEventArgs> SwatterLocationChanged;

        void MoveFly(int x, int y);
        void EnableTimer(bool enabled);
        void SetTimeLeft(TimeSpan left);
    }
}
