﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NetworkServicesLayer.Events;

using UserInterfaceLayer.Events;


namespace UserInterfaceLayer.Views
{
    public interface IGameServerView : IView
    {
        event Action TimerTick;
        event EventHandler<FlyDragStartedEventArgs> FlyDragStarted;
        event EventHandler<FlyMovingEventArgs> FlyMoving;
        event EventHandler<FlyLocationChangedEventArgs> FlyLocationChanged;

        void MoveFlyTo(int x, int y);
        void MoveFly(int deltaX, int deltaY);
        void MoveSwatter(int x, int y);
    }
}
