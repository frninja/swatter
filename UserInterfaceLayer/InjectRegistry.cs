﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows.Forms;

using StructureMap.Configuration.DSL;

using ApplicationServicesLayer;
using NetworkServicesLayer;
using UserInterfaceLayer.Views;
using UserInterfaceLayer.Presenters;
using UserInterfaceLayer.Forms;

namespace UserInterfaceLayer
{
    public class InjectRegistry : Registry
    {
        public InjectRegistry()
        {
            For<IGameStartView>().Singleton().Use<GameStartForm>();
            For<IGameClientSettingsView>().Singleton().Use<GameClientSettingsForm>();
            For<IGameServerSettingsView>().Singleton().Use<GameServerSettingsForm>();
            For<IGameServerView>().Singleton().Use<GameServerForm>();
            For<IGameClientView>().Singleton().Use<GameClientForm>();

            For<IGameStartPresenter>().Singleton().Use<GameStartPresenter>();
            For<IGameClientSettingsPresenter>().Singleton().Use<GameClientSettingsPresenter>();
            For<IGameServerSettingsPresenter>().Singleton().Use<GameServerSettingsPresenter>();
            For<IGameServerPresenter>().Singleton().Use<GameServerPresenter>();
            For<IGameClientPresenter>().Singleton().Use<GameClientPresenter>();

            For<IGameServer>().Singleton().Use<GameServer>();
            For<IGameClient>().Singleton().Use<GameClient>();
        }
    }
}
