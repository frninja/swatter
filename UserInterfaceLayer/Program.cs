﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

using ApplicationServicesLayer;
using UserInterfaceLayer.Presenters;

namespace UserInterfaceLayer
{
    static class Program
    {
        private static readonly ApplicationContext Context = new ApplicationContext();

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            IApplicationController controller = new ApplicationController();

            controller.Configure(x => x.For<ApplicationContext>().Singleton().Use<ApplicationContext>(Context));

            controller.Configure(x => x.AddRegistry(new InjectRegistry()));

            IGameStartPresenter presenter = controller.GetInstance<IGameStartPresenter>();
            presenter.Show();
            presenter.Activate();

            Application.Run(Context);
        }
    }
}
