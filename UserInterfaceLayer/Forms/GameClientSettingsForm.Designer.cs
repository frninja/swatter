﻿namespace UserInterfaceLayer.Forms
{
    partial class GameClientSettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._ipLabel = new System.Windows.Forms.Label();
            this._ipTextBox = new System.Windows.Forms.TextBox();
            this._portLabel = new System.Windows.Forms.Label();
            this._portTextBox = new System.Windows.Forms.TextBox();
            this._logTextBox = new System.Windows.Forms.TextBox();
            this._connectButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // _ipLabel
            // 
            this._ipLabel.Location = new System.Drawing.Point(12, 20);
            this._ipLabel.Name = "_ipLabel";
            this._ipLabel.Size = new System.Drawing.Size(84, 23);
            this._ipLabel.TabIndex = 0;
            this._ipLabel.Text = "Server IP:";
            // 
            // _ipTextBox
            // 
            this._ipTextBox.Location = new System.Drawing.Point(71, 20);
            this._ipTextBox.Name = "_ipTextBox";
            this._ipTextBox.Size = new System.Drawing.Size(100, 20);
            this._ipTextBox.TabIndex = 1;
            // 
            // _portLabel
            // 
            this._portLabel.Location = new System.Drawing.Point(177, 20);
            this._portLabel.Name = "_portLabel";
            this._portLabel.Size = new System.Drawing.Size(84, 23);
            this._portLabel.TabIndex = 2;
            this._portLabel.Text = "Port:";
            // 
            // _portTextBox
            // 
            this._portTextBox.Location = new System.Drawing.Point(207, 20);
            this._portTextBox.Name = "_portTextBox";
            this._portTextBox.Size = new System.Drawing.Size(54, 20);
            this._portTextBox.TabIndex = 3;
            // 
            // _logTextBox
            // 
            this._logTextBox.Location = new System.Drawing.Point(8, 46);
            this._logTextBox.Multiline = true;
            this._logTextBox.Name = "_logTextBox";
            this._logTextBox.ReadOnly = true;
            this._logTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._logTextBox.Size = new System.Drawing.Size(260, 165);
            this._logTextBox.TabIndex = 4;
            // 
            // _connectButton
            // 
            this._connectButton.Location = new System.Drawing.Point(8, 219);
            this._connectButton.Name = "_connectButton";
            this._connectButton.Size = new System.Drawing.Size(260, 23);
            this._connectButton.TabIndex = 5;
            this._connectButton.Text = "Connect";
            this._connectButton.UseVisualStyleBackColor = true;
            // 
            // ClientSettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(276, 254);
            this.Controls.Add(this._connectButton);
            this.Controls.Add(this._logTextBox);
            this.Controls.Add(this._portTextBox);
            this.Controls.Add(this._portLabel);
            this.Controls.Add(this._ipTextBox);
            this.Controls.Add(this._ipLabel);
            this.Name = "ClientSettingsForm";
            this.Text = "Client settings";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label _ipLabel;
        private System.Windows.Forms.TextBox _ipTextBox;
        private System.Windows.Forms.Label _portLabel;
        private System.Windows.Forms.TextBox _portTextBox;
        private System.Windows.Forms.TextBox _logTextBox;
        private System.Windows.Forms.Button _connectButton;
    }
}