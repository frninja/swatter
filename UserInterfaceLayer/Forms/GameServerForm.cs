﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using NetworkServicesLayer.Events;

using UserInterfaceLayer.Events;
using UserInterfaceLayer.Views;



namespace UserInterfaceLayer.Forms
{
    public partial class GameServerForm : Form, IGameServerView
    {
        private readonly ApplicationContext _context;

        private Point _flyOldPosition;

        public event Action TimerTick;
        public event EventHandler<FlyDragStartedEventArgs> FlyDragStarted;
        public event EventHandler<FlyMovingEventArgs> FlyMoving;
        public event EventHandler<FlyLocationChangedEventArgs> FlyLocationChanged;

        public GameServerForm(ApplicationContext context)
        {
            _context = context;
            InitializeComponent();
        }

        public new void Show()
        {
            _context.MainForm = this;
            base.Show();
        }

        public new void Close()
        {
            base.Close();
        }

        public void ShowError(string errorMessage)
        {
            MessageBox.Show(errorMessage);
        }

        public void ShowError(string errorMessage, string errorCaption)
        {
            MessageBox.Show(errorMessage, errorCaption);
        }

        public void MoveFlyTo(int x, int y)
        {
            _flyLabel.Location = new Point(x, y);
        }

        public void MoveFly(int deltaX, int deltaY)
        {
            Size delta = new System.Drawing.Size(deltaX - _flyOldPosition.X, deltaY - _flyOldPosition.Y);
            _flyLabel.Location += delta;
        }

        public void MoveSwatter(int x, int y)
        {
            _swatterLabel.Location = new Point(x, y);
        }

        private void _flyLabel_MouseDown(object sender, MouseEventArgs e)
        {
            //Console.WriteLine("[Mouse down] Fly pos: {0} {1}", e.X, e.Y);
            _flyOldPosition = new Point(e.X, e.Y);

            if (FlyDragStarted != null)
            {
                FlyDragStarted(this, new FlyDragStartedEventArgs(e.X, e.Y));
            }
        }

        private void _flyLabel_MouseMove(object sender, MouseEventArgs e)
        {
            if (FlyMoving != null)
            {
                if (e.Button == System.Windows.Forms.MouseButtons.Left)
                {
                    //Console.WriteLine("[Mouse move] Fly pos: {0} {1}", e.X, e.Y);
                    FlyMoving(this, new FlyMovingEventArgs(e.X, e.Y));
                }
            }
        }

        private void _flyLabel_LocationChanged(object sender, EventArgs e)
        {
            if (FlyLocationChanged != null)
            {
                FlyLocationChanged(this, new FlyLocationChangedEventArgs(_flyLabel.Location.X, _flyLabel.Location.Y));
            }
        }

        private void _timer_Tick(object sender, EventArgs e)
        {
            if (TimerTick != null)
            {
                TimerTick();
            }
        }


    }
}
