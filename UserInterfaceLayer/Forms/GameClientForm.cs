﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using NetworkServicesLayer.Events;

using UserInterfaceLayer.Events;
using UserInterfaceLayer.Views;

namespace UserInterfaceLayer.Forms
{
    public partial class GameClientForm : Form, IGameClientView
    {
        private readonly ApplicationContext _context;

        private bool _activated;

        private delegate void MoveFlyDelegate(int x, int y);

        public event Action TimerTick;
        public event Action ClientGameStarted;
        public event EventHandler<ClientWonEventArgs> ClientWon;
        public event EventHandler<SwatterLocationChangedEventArgs> SwatterLocationChanged;



        //private MoveFlyDelegate _MoveFlyDelegate;

        public GameClientForm(ApplicationContext context)
        {
            _context = context;

            //_MoveFlyDelegate = new MoveFlyDelegate(this.MoveFly_Delegate);

            InitializeComponent();
        }

        public new void Show()
        {
            _context.MainForm = this;
            base.Show();
        }

        public new void Close()
        {
            base.Close();
        }

        public void ShowError(string errorMessage)
        {
            MessageBox.Show(errorMessage);
        }

        public void ShowError(string errorMessage, string errorCaption)
        {
            MessageBox.Show(errorMessage, errorCaption);
        }

        public void MoveFly(int x, int y)
        {
            if (_flyLabel.InvokeRequired)
            {
                _flyLabel.Invoke(new MoveFlyDelegate(MoveFlyUnsafe), x, y);
            }
            else
            {
                MoveFlyUnsafe(x, y);
            }
        }

        public void EnableTimer(bool enabled)
        {
            _timer.Enabled = enabled;
        }

        public void SetTimeLeft(TimeSpan left)
        {
            _timeLeftLabel.Text = left.Seconds.ToString();
        }

        private void MoveFlyUnsafe(int x, int y)
        {
            //Size sz = new System.Drawing.Size(x - _flyLabel.Location.X, y - _flyLabel.Location.Y);
            //_flyLabel.Location += sz;

            _flyLabel.Location = new Point(x, y);
            _flyLabel.Update();
        }

        private void GameClientForm_MouseMove(object sender, MouseEventArgs e)
        {
            
        }

        private void _flyLabel_MouseMove(object sender, MouseEventArgs e)
        {
            //Console.WriteLine("[SWATTER MOVE FLY] {0} {1}", _flyLabel.Location.X + e.X, _flyLabel.Location.Y + e.Y);
            if (SwatterLocationChanged != null)
            {
                int swatterX = _flyLabel.Location.X + e.X;
                int swatterY = _flyLabel.Location.Y + e.Y;

                if (_activated)
                    SwatterLocationChanged(this, new SwatterLocationChangedEventArgs(swatterX, swatterY));
            }
        }

        private void _flyLabel_Click(object sender, EventArgs e)
        {
            if (ClientWon != null)
            {
                ClientWon(this, new ClientWonEventArgs());
            }
        }


        private void GameClientForm_Load(object sender, EventArgs e)
        {
            if (ClientGameStarted != null)
            {
                ClientGameStarted();
            }
        }


        private void GameClientForm_Activated(object sender, EventArgs e)
        {
            _activated = true;
        }

        private void GameClientForm_Deactivate(object sender, EventArgs e)
        {
            _activated = false;
        }

        private void _timer_Tick(object sender, EventArgs e)
        {
            if (TimerTick != null)
            {
                TimerTick();
            }
        }

        private void _panel_MouseMove(object sender, MouseEventArgs e)
        {
            //Console.WriteLine("[SWATTER MOVE] {0} {1}", e.X, e.Y);
            if (SwatterLocationChanged != null)
            {
                if (_activated)
                    SwatterLocationChanged(this, new SwatterLocationChangedEventArgs(e.X, e.Y));
            }
        }

    }
}
