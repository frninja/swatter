﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using UserInterfaceLayer.Events;
using UserInterfaceLayer.Views;

namespace UserInterfaceLayer.Forms
{
    public partial class GameStartForm : Form, IGameStartView
    {
        private readonly ApplicationContext _context;

        public event EventHandler<GameRoleSelectedEventArgs> GameRoleSelected;

        public GameStartForm(ApplicationContext context)
        {
            _context = context;
            InitializeComponent();
        }

        public new void Show()
        {
            _context.MainForm = this;
            base.Show();
        }

        public new void Close()
        {
            base.Close();
        }

        public void ShowError(string errorMessage)
        {
            MessageBox.Show(errorMessage);
        }

        public void ShowError(string errorMessage, string errorCaption)
        {
            MessageBox.Show(errorMessage, errorCaption);
        }

        private void _serverButton_Click(object sender, EventArgs e)
        {
            if (GameRoleSelected != null)
            {
                Button button = sender as Button;
                GameRole role = sender.Equals(_serverButton) ? GameRole.Server : GameRole.Client;
                GameRoleSelected(this, new GameRoleSelectedEventArgs(role));
            }
        }
    }
}
