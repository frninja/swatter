﻿namespace UserInterfaceLayer.Forms
{
    partial class GameClientForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this._flyLabel = new System.Windows.Forms.Label();
            this._timer = new System.Windows.Forms.Timer(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this._timerLabel = new System.Windows.Forms.Label();
            this._timeLeftLabel = new System.Windows.Forms.Label();
            this._panel = new System.Windows.Forms.Panel();
            this._panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // _flyLabel
            // 
            this._flyLabel.BackColor = System.Drawing.SystemColors.WindowFrame;
            this._flyLabel.Location = new System.Drawing.Point(61, 36);
            this._flyLabel.Name = "_flyLabel";
            this._flyLabel.Size = new System.Drawing.Size(100, 23);
            this._flyLabel.TabIndex = 1;
            this._flyLabel.Text = "Муха";
            this._flyLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this._flyLabel.Click += new System.EventHandler(this._flyLabel_Click);
            this._flyLabel.MouseMove += new System.Windows.Forms.MouseEventHandler(this._flyLabel_MouseMove);
            // 
            // _timer
            // 
            this._timer.Tick += new System.EventHandler(this._timer_Tick);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(284, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // _timerLabel
            // 
            this._timerLabel.AutoSize = true;
            this._timerLabel.Location = new System.Drawing.Point(129, 2);
            this._timerLabel.Name = "_timerLabel";
            this._timerLabel.Size = new System.Drawing.Size(46, 13);
            this._timerLabel.TabIndex = 3;
            this._timerLabel.Text = "Время: ";
            // 
            // _timeLeftLabel
            // 
            this._timeLeftLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._timeLeftLabel.Location = new System.Drawing.Point(181, 1);
            this._timeLeftLabel.Name = "_timeLeftLabel";
            this._timeLeftLabel.Size = new System.Drawing.Size(100, 20);
            this._timeLeftLabel.TabIndex = 4;
            // 
            // _panel
            // 
            this._panel.Controls.Add(this._flyLabel);
            this._panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._panel.Location = new System.Drawing.Point(0, 24);
            this._panel.Name = "_panel";
            this._panel.Size = new System.Drawing.Size(284, 238);
            this._panel.TabIndex = 5;
            this._panel.MouseMove += new System.Windows.Forms.MouseEventHandler(this._panel_MouseMove);
            // 
            // GameClientForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this._panel);
            this.Controls.Add(this._timeLeftLabel);
            this.Controls.Add(this._timerLabel);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "GameClientForm";
            this.Text = "Game - Client";
            this.Activated += new System.EventHandler(this.GameClientForm_Activated);
            this.Deactivate += new System.EventHandler(this.GameClientForm_Deactivate);
            this.Load += new System.EventHandler(this.GameClientForm_Load);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.GameClientForm_MouseMove);
            this._panel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label _flyLabel;
        private System.Windows.Forms.Timer _timer;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.Label _timerLabel;
        private System.Windows.Forms.Label _timeLeftLabel;
        private System.Windows.Forms.Panel _panel;
    }
}