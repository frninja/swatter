﻿namespace UserInterfaceLayer.Forms
{
    partial class GameStartForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._groupBox = new System.Windows.Forms.GroupBox();
            this._serverButton = new System.Windows.Forms.Button();
            this._clientButton = new System.Windows.Forms.Button();
            this._groupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // _groupBox
            // 
            this._groupBox.Controls.Add(this._clientButton);
            this._groupBox.Controls.Add(this._serverButton);
            this._groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this._groupBox.Location = new System.Drawing.Point(0, 0);
            this._groupBox.Name = "_groupBox";
            this._groupBox.Size = new System.Drawing.Size(284, 262);
            this._groupBox.TabIndex = 0;
            this._groupBox.TabStop = false;
            this._groupBox.Text = "Start game as:";
            // 
            // _serverButton
            // 
            this._serverButton.Location = new System.Drawing.Point(6, 19);
            this._serverButton.Name = "_serverButton";
            this._serverButton.Size = new System.Drawing.Size(266, 117);
            this._serverButton.TabIndex = 0;
            this._serverButton.Text = "Server";
            this._serverButton.UseVisualStyleBackColor = true;
            this._serverButton.Click += new System.EventHandler(this._serverButton_Click);
            // 
            // _clientButton
            // 
            this._clientButton.Location = new System.Drawing.Point(6, 142);
            this._clientButton.Name = "_clientButton";
            this._clientButton.Size = new System.Drawing.Size(266, 108);
            this._clientButton.TabIndex = 1;
            this._clientButton.Text = "Client";
            this._clientButton.UseVisualStyleBackColor = true;
            this._clientButton.Click += new System.EventHandler(this._serverButton_Click);
            // 
            // GameStartForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this._groupBox);
            this.Name = "GameStartForm";
            this.Text = "Start game";
            this._groupBox.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox _groupBox;
        private System.Windows.Forms.Button _serverButton;
        private System.Windows.Forms.Button _clientButton;
    }
}