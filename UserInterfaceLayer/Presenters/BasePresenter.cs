﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ApplicationServicesLayer;
using UserInterfaceLayer.Views;
using UserInterfaceLayer.Presenters;

namespace UserInterfaceLayer.Presenters
{
    public abstract class BasePresenter<TView> : IPresenter
        where TView : IView
    {
        protected IApplicationController Controller { get; private set; }
        protected TView View { get; private set; }

        public BasePresenter(IApplicationController controller, TView view)
        {
            Controller = controller;
            View = view;
        }

        public void Show()
        {
            View.Show();
        }

        public void Hide()
        {
            View.Hide();
        }

        public void Activate()
        {
            View.Activate();
        }

        public void Close()
        {
            View.Close();
        }
    }
}
