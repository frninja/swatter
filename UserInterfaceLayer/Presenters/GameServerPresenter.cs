﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ApplicationServicesLayer;

using NetworkServicesLayer;
using NetworkServicesLayer.Events;

using UserInterfaceLayer.Events;
using UserInterfaceLayer.Views;

namespace UserInterfaceLayer.Presenters
{
    public class GameServerPresenter : BasePresenter<IGameServerView>, IGameServerPresenter
    {
        private IGameServer _server;

        private Position _flyPosition;

        public GameServerPresenter(IApplicationController controller, IGameServerView view) :
            base(controller, view)
        {
            View.FlyDragStarted += OnFlyDragStarted;
            View.FlyMoving += OnFlyPositionChanged;
            View.FlyLocationChanged += OnFlyLocationChanged;
        }

        public void Initialize(IGameServer server)
        {
            _server = server;
            _server.ClientGameStarted += OnClientGameStarted;
            _server.ClientWon += OnClientWon;
            _server.SwatterLocationChanged += OnSwatterLocationChanged;
        }

        private void OnClientGameStarted()
        {
            View.MoveFlyTo(0, 0);
        }

        private void OnClientWon(object sender, ClientWonEventArgs e)
        {
            View.ShowError("You lose", "Loose, Carl!");
        }

        private void OnFlyDragStarted(object sender, FlyDragStartedEventArgs e)
        {
            _flyPosition = new Position(e.X, e.Y);
        }

        private void OnFlyPositionChanged(object sender, FlyMovingEventArgs e)
        {
            int flyX = _flyPosition.X + e.X;
            int flyY = _flyPosition.Y + e.Y;

            Console.WriteLine("[FLY DELTA] {0} {1}", e.X, e.Y);

            View.MoveFly(e.X, e.Y);
            
        }

        private void OnFlyLocationChanged(object sender, FlyLocationChangedEventArgs e)
        {
            _server.SendMessage(CommandFactory.MakeFlyMoveCommand(e.X, e.Y));
        }

        private void OnSwatterLocationChanged(object sender, SwatterLocationChangedEventArgs e)
        {
            View.MoveSwatter(e.X, e.Y);
        }
    }
}
