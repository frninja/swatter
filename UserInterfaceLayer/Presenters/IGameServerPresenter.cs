﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NetworkServicesLayer;

namespace UserInterfaceLayer.Presenters
{
    public interface IGameServerPresenter : IPresenter
    {
        void Initialize(IGameServer server);
    }
}
