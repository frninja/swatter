﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ApplicationServicesLayer;
using UserInterfaceLayer.Events;
using UserInterfaceLayer.Views;

using NetworkServicesLayer;

namespace UserInterfaceLayer.Presenters
{
    public class GameStartPresenter : BasePresenter<IGameStartView>, IGameStartPresenter
    {
        public GameStartPresenter(IApplicationController controller, IGameStartView view)
            : base(controller, view)
        {
            View.GameRoleSelected += OnGameRoleSelected;
        }

        private void OnGameRoleSelected(object sender, GameRoleSelectedEventArgs e)
        {
            switch (e.Role)
            {
                case GameRole.Server:
                    IGameServerSettingsPresenter serverSettingsPresenter = Controller.GetInstance<IGameServerSettingsPresenter>();
                    serverSettingsPresenter.Initialize();
                    serverSettingsPresenter.Show();
                    serverSettingsPresenter.Activate();

                    /*IGameServer server = Controller.GetInstance<IGameServer>();
                    server.Initialize();

                    IGameServerPresenter serverPresenter = Controller.GetInstance<IGameServerPresenter>();
                    serverPresenter.Initialize(server);
                    serverPresenter.Show();
                    serverPresenter.Activate();*/

                    break;
                case GameRole.Client:
                    IGameClientSettingsPresenter clientSettingsPresenter = Controller.GetInstance<IGameClientSettingsPresenter>();
                    clientSettingsPresenter.Initialize();
                    clientSettingsPresenter.Show();
                    clientSettingsPresenter.Activate();

                    /*IGameClient client = Controller.GetInstance<IGameClient>();
                    client.Initialize(System.Net.IPAddress.Parse("127.0.0.1"), 55783);

                    IGameClientPresenter clientPresenter = Controller.GetInstance<IGameClientPresenter>();
                    clientPresenter.Initialize(client);
                    clientPresenter.Show();
                    clientPresenter.Activate();*/

                    //System.Threading.Thread.Sleep(3000);
                    //client.SendMessage("YO ETO TI?");
                    //System.Threading.Thread.Sleep(10000);
                    //client.SendMessage("HEEEEY");
                    break;
            }
        }
    }
}
