﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Net;
using System.Net.Sockets;
using System.Net.Http;

using ApplicationServicesLayer;
using UserInterfaceLayer.Views;


namespace UserInterfaceLayer.Presenters
{
    public class GameServerSettingsPresenter : BasePresenter<IGameServerSettingsView>, IGameServerSettingsPresenter
    {
        public GameServerSettingsPresenter(IApplicationController controller, IGameServerSettingsView view)
            : base(controller, view)
        {
        }

        public void Initialize()
        {
            Task task = new TaskFactory().StartNew(this.RunIpDetectionService);

            this.GetExternalIP();

            //Console.WriteLine("[EXTERNAL IP] {0}", ip);
        }

        private async void GetExternalIP()
        {
            string ipString = "127.0.0.1";

            /*ipString = Dns.GetHostEntry(Dns.GetHostName()).AddressList
                .Where(ip => ip.AddressFamily == AddressFamily.InterNetwork)
                .Select(ip => ip.ToString())
                .FirstOrDefault() ?? "";*/

            WebClient client = new WebClient();
            string ip = client.DownloadString("http://localhost:56777/PublicIP");

            Console.WriteLine(ip);

            /*string url = "http://checkip.dyndns.org";
            System.Net.WebRequest req = System.Net.WebRequest.Create(url);
            System.Net.WebResponse resp = req.GetResponse();
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            string response = sr.ReadToEnd().Trim();
            string[] a = response.Split(':');
            string a2 = a[1].Substring(1);
            string[] a3 = a2.Split('<');
            string a4 = a3[0];

            Console.WriteLine(a4);*/
        }

        private void RunIpDetectionService()
        {
            HttpListener listener = new HttpListener();

            listener.Prefixes.Add("http://localhost:56777/PublicIP/");

            listener.Start();

            while (true)
            {
                HttpListenerContext context = listener.GetContext();
                string clientIP = context.Request.Headers.ToString();
                using (var response = context.Response.OutputStream)
                using (var writer = new System.IO.StreamWriter(response))
                    writer.Write(clientIP);

                context.Response.Close();
            }
        }
    }
}
