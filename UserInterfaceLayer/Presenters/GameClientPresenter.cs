﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ApplicationServicesLayer;

using NetworkServicesLayer;
using NetworkServicesLayer.Events;

using UserInterfaceLayer.Events;
using UserInterfaceLayer.Views;

namespace UserInterfaceLayer.Presenters
{
    public class GameClientPresenter : BasePresenter<IGameClientView>, IGameClientPresenter
    {
        private IGameClient _client;

        private DateTime _finishTime;

        public GameClientPresenter(IApplicationController controller, IGameClientView view)
            : base(controller, view)
        {
            View.ClientGameStarted += OnClientGameStarted;
            View.TimerTick += OnTimerTick;
            View.ClientWon += OnClientWon;
            View.SwatterLocationChanged += OnSwatterLocationChanged;
        }

        public void Initialize(IGameClient client)
        {
            _client = client;
            _client.FlyLocationChanged += OnFlyLocationChanged;
        }

        private void OnClientGameStarted()
        {
            View.EnableTimer(true);

            _finishTime = DateTime.Now.AddMinutes(1);

            _client.SendMessage(CommandFactory.MakeClientGameStartedCommand());
        }

        private void OnClientWon(object sender, ClientWonEventArgs e)
        {
            _client.SendMessage(CommandFactory.MakeClientWonCommand());

            // Use ShowMessage instead
            View.ShowError("You won!", "Congratulations!");
        }

        private void OnFlyLocationChanged(object sender, FlyLocationChangedEventArgs e)
        {
            // sth
            View.MoveFly(e.X, e.Y);
        }

        private void OnSwatterLocationChanged(object sender, SwatterLocationChangedEventArgs e)
        {
            _client.SendMessage(CommandFactory.MakeSwatterMoveCommand(e.X, e.Y));
        }

        private void OnTimerTick()
        {
            TimeSpan timeLeft = _finishTime - DateTime.Now;

            if (timeLeft.Seconds == 0)
            {
                View.EnableTimer(false);
            }

            View.SetTimeLeft(timeLeft);
        }

        
    }
}
