﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserInterfaceLayer.Presenters
{
    public interface IGameServerSettingsPresenter : IPresenter
    {
        void Initialize();
    }
}
