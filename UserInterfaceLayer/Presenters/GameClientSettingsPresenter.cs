﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ApplicationServicesLayer;
using UserInterfaceLayer.Views;

namespace UserInterfaceLayer.Presenters
{
    public class GameClientSettingsPresenter : BasePresenter<IGameClientSettingsView>, IGameClientSettingsPresenter
    {
        public GameClientSettingsPresenter(IApplicationController controller, IGameClientSettingsView view)
            : base(controller, view)
        {

        }

        public void Initialize()
        {

        }
    }
}
